## Servita DevOps Mini Challenge

## Background

A popular media streaming service receives sequentially numbered 5 second fragments of MPEG video for storage and rebroadcast via a CDN.

The Ingest integration point is a managed S3 bucket, where the media Provider has been granted write access.

An agreed schema using filenames (keys) has been set up where the chunks are labelled with some basic metadata, following the format

`string-id_target-resolution_target-bitrate.sequence_number.ts`

# Example Files Added to Bucket

```
unique-show-identifier_1080p_2048.0.ts
unique-show-identifier_1080p_2048.1.ts
unique-show-identifier_1080p_2048.2.ts
unique-show-identifier_1080p_2048.3.ts

unique-show-identifier_320p_256.0.ts
unique-show-identifier_320p_256.1.ts
unique-show-identifier_320p_256.2.ts
unique-show-identifier_320p_256.3.ts
```

## Task

![Solution Overview](./Challenge.excalidraw.svg)

In order to play back these *shows*, a playlist will need to be created for each Programme/Show ID:

* at each Target resolution
* at each Target bitrate

```txt
# wonderful-show_480p_1500.playlist

wonderful-show_1080p_2048.1.ts
empty.ts
wonderful-show_1080p_2048.3.ts
wonderful-show_1080p_2048.4.ts
empty.ts
empty.ts
wonderful-show_1080p_2048.7.ts
wonderful-show_1080p_2048.8.ts
```

As a stretch goal (not required but a nice-to-have):
A table of contents should be built for each Media-ID to link to the different Playlist Files

eg
```txt
# wonderful-show.toc

./wonderful-show_480p_1500.playlist
./wonderful-show_480p_3000.playlist
./wonderful-show_720p_5000.playlist
```

Rules:

* The system must support *live programming events* - (eg sports)
* Media fragments can arrive in *any order*
* A single *show* can have many different target resolutions
* A single *show* can have many different target bitrates
* A single *show* can be any *duration* but will not exceed `65535` total segments (eg will not be longer than ~90 hours total)
* If a segment is missing (eg, we have `1.ts` `2.ts` and `5.ts`) the missing segments must be replaced by a dummy file which will fill the gap until it can be replaced with the missing segments. (assume this will be a file called `empty.ts`)
* If a segment *remains* missing for some duration (eg it hasn't arrived within 30 minutes), we will need to contact the provider and ask they re-send the missing piece.

The Output files:

* A playlist file is an *ordered list* of media filenames, seperated by line breaks
* A table of contents file is a list of all playlist filenames that relate to a single piece of media
* There is currently no specification for the folder structure of the output, we would welcome a proposal
* There should be no need to move the incoming/original MPEG fragments, they will be uploaded to a CDN from which viewers can access them.

Explicitly Out of Scope:

* Security/Authentication
* CDN

### What we would like to see

Assuming the only resource that exists currently is a single S3 bucket, can you provide a solution which will build the necessary playlists, and table-of-contents files as the fragments arrive?

We've designed this challenge to be *fairly* realistic. Please approach this like we are the Product Owners and this brief has been assigned to you. Please note we *will* be available for a brief clarification/3-amigos/work-ingest call.

### Considerations

In this prototype, we're looking to understand what approaches you would consider; and how you begin to execute this plan. Please be prepared to talk through your solution, saving your diagrams and notes you take on the way.

Whilst we're open to seeing solutions rendered using any combination of Infrastructure-As-Code and programming languages/paradigms its worth noting that in our existing stack we typically make use of:

* Hashicorp Terraform
* Python
* NodeJS
* AWS Resources

This challenge should be possible to complete with AWS resources available on the free tier or with a sub $5 monthly spend. If assistance is required in setting up a cloud environment for this challenge we can provide that (within reason).
